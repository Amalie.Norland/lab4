package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    /** Number of columns */
	private int columns;
    /** Number of rows */
	private int rows;
    /** State of cells */
    private CellState initialState;
    /** Structure containing rows*cols CellState-values */
    CellState[][] grid;


    public CellGrid(int rows, int columns, CellState initialState) {
        this.rows = rows;
        this.columns = columns;
        this.initialState = initialState;
        this.grid = new CellState[rows][columns];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                this.grid[i][j] = initialState;
            }
        }
	}

    @Override
    public int numRows() {
        return this.rows;
    }

    @Override
    public int numColumns() {
        return this.columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if ((0 <= row  &&  row <= rows) && (0 <= column  &&  column <= columns)) {
            this.grid[row][column] = element;
        }
        else {
            throw new IndexOutOfBoundsException("Index out of range");
        }

    }

    @Override
    public CellState get(int row, int column) {
        if ((0 <= row  &&  row <= rows) && (0 <= column  &&  column <= columns)) {
            CellState state = this.grid[row][column];
            return state;
        }
        else {
            throw new IndexOutOfBoundsException("Index out of range");
        }
    }

    @Override
    public IGrid copy() {
        IGrid copiedGrid = new CellGrid(this.rows, this.columns, this.initialState);
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                copiedGrid.set(i, j, this.get(i,j));
            }
        }
        return copiedGrid;
    }
    
}
